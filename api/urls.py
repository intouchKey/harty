from django.urls import path, include
from rest_framework import routers, permissions
from django.conf import settings

from rest_framework.decorators import api_view

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from api.routes.AuthRoute import authPatterns
from api.routes.UserRoute import userPatterns
from api.routes.PartyRoute import partyPatterns
from api.routes.ActivityRoute import activityPatterns

schema_view = get_schema_view(
   openapi.Info(
      title="Harty API",
      default_version=settings.VERSION,
      description="Harty API description",
      contact=openapi.Contact(email="keyeducation2544@gmail.com"),
      license=openapi.License(name="Harty License"),
   ),
   public=False,
   permission_classes=(permissions.IsAuthenticated,),
)

urlpatterns = [
   path('doc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-swagger-ui')
]

urlpatterns += authPatterns
urlpatterns += userPatterns
urlpatterns += partyPatterns
urlpatterns += activityPatterns
