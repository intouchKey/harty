from django import forms
from django.core import validators

class UserValidationForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    age = forms.IntegerField(validators = [validators.MinValueValidator(0), validators.MaxValueValidator(100)])
    gender = forms.IntegerField(validators = [validators.MinValueValidator(0), validators.MaxValueValidator(2)] )

