from django import forms
from django.core import validators

class PartyRequestValidationForm(forms.Form):
    user_id = forms.IntegerField()
    party_id = forms.IntegerField()

