from django import forms
from django.core import validators

class PartyDeclineRequestValidationForm(forms.Form):
    user_id = forms.IntegerField()
    declined_user_id = forms.IntegerField()
    party_id = forms.IntegerField()
