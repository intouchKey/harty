from django import forms
from django.core import validators

class PartyAcceptRequestValidationForm(forms.Form):
    user_id = forms.IntegerField()
    accepted_user_id = forms.IntegerField()
    party_id = forms.IntegerField()
