from django import forms
from django.core import validators

class PartyValidationForm(forms.Form):
    name = forms.CharField()
    capacity = forms.IntegerField(validators = [validators.MinValueValidator(2), validators.MaxValueValidator(10)])
    description = forms.CharField()
    location_description = forms.CharField()
    date = forms.CharField()
    expiration_date = forms.CharField()
    created_user = forms.CharField()
    locked_activity = forms.IntegerField()
    province = forms.CharField()
    user_id = forms.IntegerField()

