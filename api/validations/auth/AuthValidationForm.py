from django import forms
from django.core import validators

class LoginValidationForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(validators = [validators.MinLengthValidator(6)])

class RegisterValidationForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(validators = [validators.MinLengthValidator(6)])
    email = forms.EmailField(validators = [validators.EmailValidator()])
    first_name = forms.CharField(validators = [validators.MinLengthValidator(1)])
    last_name = forms.CharField(validators = [validators.MinLengthValidator(1)])
    age = forms.IntegerField(validators = [validators.MinValueValidator(0), validators.MaxValueValidator(100)])
    gender = forms.IntegerField(validators = [validators.MinValueValidator(0), validators.MaxValueValidator(2)] )
