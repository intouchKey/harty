from django.db import models

class User(models.Model):
    first_name = models.CharField(max_length=20, null=False, blank=False)
    last_name = models.CharField(max_length=20, null=False, blank=False)
    gender = models.IntegerField(null=False)
    age = models.IntegerField(null=False)
    line_id = models.CharField(max_length=100, null=True, blank=False)
    facebook_name = models.CharField(max_length=100, null=True, blank=False)
    telephone_number = models.CharField(max_length=10, null=True, blank=False)

    def getId(self):
        return self.id

    def getData(self):
        return {
            "id": self.id,
            "firstName": self.first_name,
            "lastName": self.last_name,
            "gender": self.gender,
            "age": self.age,
            "lineId": self.line_id,
            "facebookName": self.facebook_name,
            "telephoneNumber": self.telephone_number
        }
