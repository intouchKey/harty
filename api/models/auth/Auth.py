from django.db import models
from api.models.user.User import User

class Auth(models.Model):
    username = models.CharField(max_length=16, null=False, blank=False, unique=True)
    password = models.TextField(null=False, blank=False)
    email = models.TextField(null=False, blank=False, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    verification_code = models.TextField(null=False, blank=False)
    verified = models.BooleanField(default=False, null=False, blank=False)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.username