from django.db import models

class Province(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)

    def getData(self):
        return {
            "id": self.id,
            "name": self.name
        }    
