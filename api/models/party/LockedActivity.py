from django.db import models
from api.models.party.Category import Category
from api.models.party.Province import Province
from api.models.party.Picture import Picture

class LockedActivity(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    province = models.ForeignKey(
        Province,
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
    )
    picture = models.ForeignKey(
        Picture,
        on_delete=models.CASCADE,
    )
    location_description = models.TextField()
    description = models.TextField()
    capacity = models.IntegerField(null=False)
    promotion = models.TextField()

    def getData(self):
        return {
            "id": self.id,
            "name": self.name,
            "province": self.province.getData(),
            "category": self.category.getData(),
            "picture": self.picture.getData(),
            "location_description": self.location_description,
            "description": self.description,
            "capacity": self.capacity,
            "promotion": self.promotion
        }    