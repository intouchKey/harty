from django.db import models
from api.models.user.User import User
from api.models.party.Province import Province
from api.models.party.LockedActivity import LockedActivity

class Party(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    capacity = models.IntegerField(null=False)
    members_no = models.IntegerField(default=1, null=False)
    description = models.TextField()
    location_description = models.TextField()
    is_full = models.BooleanField(default=False, null=False, blank=False)
    date = models.DateTimeField(null=False)
    expiration_date = models.DateTimeField(null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    created_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    province = models.ForeignKey(
        Province,
        on_delete=models.CASCADE,
    )
    locked_activity = models.ForeignKey(
        LockedActivity,
        on_delete=models.CASCADE,
    )

    def getData(self):
        return {
            "id": self.id,
            "name": self.name,
            "capacity": self.capacity,
            "members_no": self.members_no,
            "description": self.description,
            "location_description": self.location_description,
            "is_full": self.is_full,
            "date": self.date,
            "expiration_date": self.expiration_date,
            "date_created": self.date_created,
            "date_modified": self.date_modified,
            "created_user": self.created_user.getData(),   
            "province": self.province.getData(),
            "locked_activity": self.locked_activity.getData()
        }  
