from django.db import models
from api.models.party.Party import Party
from api.models.user.User import User

class PartyMember(models.Model):
    party = models.ForeignKey(
        Party,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
