from django.db import models

class Picture(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    url = models.TextField(null=False, blank=False)

    def getData(self):
        return {
            "id": self.id,
            "name": self.name,
            "url": self.url
        }    