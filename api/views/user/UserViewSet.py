from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.serializers.user.UserSerializer import UserSerializer

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from api.utils.common.FormValidator import FormValidator
from api.validations.user.UserValidationForm import UserValidationForm
from api.services.user.UserService import UserService
from api.utils.common.HartyResponse import HartyResponse
from api.utils.common.APITokenHandler import api_token_handler
from api.utils.common.ExceptionHandler import exception_handler
from api.utils.common.BearerTokenHandler import bearer_token_handler

class UserViewSet(ViewSet):
    serializer_class = UserSerializer

    @exception_handler
    @bearer_token_handler
    def getInfo(self, request):
        userId = request.data["user_id"]
        response = UserService.getInfo(userId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def getCreatedParties(self, request):
        userId = request.data["user_id"]
        response = UserService.getCreatedParties(userId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def getRequestedParties(self, request):
        userId = request.data["user_id"]
        response = UserService.getRequestedParties(userId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def getCreatedPartiesWithRequestedUsers(self, request):
        userId = request.data["user_id"]
        response = UserService.getCreatedPartiesWithRequestedUsers(userId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def getJoinedParties(self, request):
        userId = request.data["user_id"]
        response = UserService.getJoinedParties(userId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def updateUser(self, request):
        data = FormValidator.create(UserValidationForm, request.data)

        firstName = data["first_name"].value()
        lastName = data["last_name"].value()
        age = data["age"].value()
        gender = data["gender"].value()
        
        contactInfo = self._getContactInfo(request.data)

        response = UserService.update(
            request.data["user_id"],
            firstName, 
            lastName, 
            age, 
            gender, 
            contactInfo["line_id"], 
            contactInfo["facebook_name"],
            contactInfo["telephone_number"]
        )

        return HartyResponse.create200(response)

    def _getContactInfo(self, data):
        contactInfo = dict()

        try:
            contactInfo["line_id"] = data["line_id"]
        except:
            contactInfo["line_id"] = None

        try:
            contactInfo["facebook_name"] = data["facebook_name"]
        except:
            contactInfo["facebook_name"] = None

        try:
            contactInfo["telephone_number"] = data["telephone_number"]
        except:
            contactInfo["telephone_number"] = None

        return contactInfo

           