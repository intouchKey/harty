from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.models.party.LockedActivity import LockedActivity
from api.serializers.party.PartySerializer import PartySerializer

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from api.utils.common.FormValidator import FormValidator
from api.validations.party.PartyValidationForm import PartyValidationForm
from api.validations.party.PartyRequestValidationForm import PartyRequestValidationForm
from api.validations.party.PartyAcceptRequestValidationForm import PartyAcceptRequestValidationForm
from api.services.party.ActivityService import ActivityService
from api.utils.common.HartyResponse import HartyResponse
from api.utils.common.ExceptionHandler import exception_handler
from api.utils.common.BearerTokenHandler import bearer_token_handler

class ActivityViewSet(ViewSet):
    @exception_handler
    def getAllActivities(self, request):
        response = ActivityService.getAll()

        return HartyResponse.create200(response)
