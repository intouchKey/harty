from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.models.party.PartyMember import PartyMember
from api.models.party.PartyRequest import PartyRequest
from api.serializers.party.PartySerializer import PartySerializer

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from api.utils.common.FormValidator import FormValidator
from api.validations.party.PartyValidationForm import PartyValidationForm
from api.validations.party.PartyRequestValidationForm import PartyRequestValidationForm
from api.validations.party.PartyAcceptRequestValidationForm import PartyAcceptRequestValidationForm
from api.validations.party.PartyDeclineRequestValidationForm import PartyDeclineRequestValidationForm
from api.services.party.PartyService import PartyService
from api.utils.common.HartyResponse import HartyResponse
from api.utils.common.HartyRequest import HartyRequest
from api.utils.common.ExceptionHandler import exception_handler
from api.utils.common.BearerTokenHandler import bearer_token_handler

class PartyViewSet(ViewSet):
    serializer_class = PartySerializer

    @exception_handler
    def getAllParties(self, request):
        activityId = HartyRequest.getQueryParam('activity-id', request)
        
        if (activityId):
            response = PartyService.getByActivityId(activityId)
        else:   
            response = PartyService.getAll()

        return HartyResponse.create200(response)

    @exception_handler
    def getRequestedUsers(self, request):
        partyId = request.GET['party-id']
        response = PartyService.getRequestedUsers(partyId)

        return HartyResponse.create200(response)

    @exception_handler
    def getJoinedUsers(self, request):
        partyId = request.GET['party-id']
        response = PartyService.getJoinedUsers(partyId)

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def create(self, request):
        data = FormValidator.create(PartyValidationForm, request.data).data

        response = PartyService.create(
            data["user_id"], 
            data["name"], 
            data["capacity"], 
            data["description"], 
            data["location_description"], 
            data["date"], 
            data["expiration_date"], 
            data["province"], 
            data["locked_activity"])

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def requestToJoin(self, request):
        data = FormValidator.create(PartyRequestValidationForm, request.data).data

        response = PartyService.requestToJoin(
            data["user_id"], 
            data["party_id"], 
        )

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def acceptRequest(self, request):
        data = FormValidator.create(PartyAcceptRequestValidationForm, request.data).data

        response = PartyService.acceptRequest(
            data["user_id"], 
            data["accepted_user_id"],
            data["party_id"], 
        )

        return HartyResponse.create200(response)

    @exception_handler
    @bearer_token_handler
    def declineRequest(self, request):
        data = FormValidator.create(PartyDeclineRequestValidationForm, request.data).data

        response = PartyService.declineRequest(
            data["user_id"], 
            data["declined_user_id"],
            data["party_id"], 
        )

        return HartyResponse.create200(response)
