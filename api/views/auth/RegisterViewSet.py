from api.models.auth.Auth import Auth
from api.serializers.auth.AuthSerializer import AuthSerializer
from api.serializers.user.UserSerializer import UserSerializer

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from api.validations.auth.AuthValidationForm import RegisterValidationForm
from api.services.auth.AuthService import AuthService
from api.utils.common.FormValidator import FormValidator
from api.utils.common.HartyResponse import HartyResponse
from api.utils.common.ExceptionHandler import exception_handler
from api.utils.common.APITokenHandler import api_token_handler
from api.utils.exceptions.IntendedException import IntendedException

class RegisterViewSet(ViewSet):
    serializer_class = AuthSerializer

    @exception_handler
    @api_token_handler
    def sendVerificationCode(self, request):
        username = request.GET['username']

        response = AuthService.sendVerificationCode(username)

        return HartyResponse.create200(response)

    @exception_handler
    @api_token_handler
    def createAccount(self, request):
        data = FormValidator.create(RegisterValidationForm, request.data).data

        response = AuthService.register(
            data["username"], 
            data["password"], 
            data["email"], 
            data["first_name"], 
            data["last_name"], 
            data["age"], 
            data["gender"]
        )

        return HartyResponse.create201(response)

    @exception_handler
    @api_token_handler
    def verifyAccount(self, request):
        username = request.GET['username']
        verificationCode = request.GET['verification-code']

        response = AuthService.verify(username, verificationCode)

        return HartyResponse.create200(response)
