from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.serializers.auth.AuthSerializer import AuthSerializer

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet

from api.utils.common.FormValidator import FormValidator
from api.validations.auth.AuthValidationForm import LoginValidationForm
from api.services.auth.AuthService import AuthService
from api.utils.common.HartyResponse import HartyResponse
from api.utils.common.ExceptionHandler import exception_handler
from api.utils.common.APITokenHandler import api_token_handler

class LoginViewSet(ViewSet):
    serializer_class = AuthSerializer

    @exception_handler
    @api_token_handler
    def login(self, request):
        data = FormValidator.create(LoginValidationForm, request.data).data

        response = AuthService.login(
            data["username"], 
            data["password"]
        )

        return HartyResponse.create200(response)
