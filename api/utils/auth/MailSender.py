from django.conf import settings

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import smtplib
import ssl
import string
import random

class MailSender(object):
    @staticmethod
    def sendVerificationCode(receiverEmail, verificationCode):
        senderEmail = settings.SENDER_EMAIL
        password = settings.EMAIL_PASSWORD
        html = MailSender.getMailHtml().format(verificationCode)

        message = MIMEMultipart("alternative")
        message["Subject"] = "Harty verification code"
        message["From"] = senderEmail
        message["To"] = receiverEmail
        htmlPart = MIMEText(html, "html")

        message.attach(htmlPart)

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(senderEmail, password)
            server.sendmail(
                senderEmail, receiverEmail, message.as_string()
            )

    @staticmethod
    def generateVerificationCode(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    @staticmethod
    def getMailHtml():
        return """\
        <html>
        <body>
            <p>
            <img src="http://i.ibb.co/pJFgZrd/Screenshot-2563-08-13-at-18-23-48.png">
            </p>
            <p align="center">
                <font size="4" color="#153a63">
                    Your verification code is {}.
                </font>
            </p>
        </body>
        </html>
        """
