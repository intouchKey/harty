from passlib.hash import sha256_crypt
from django.conf import settings

import json
import jwt
import datetime

class PasswordManager(object):
    @staticmethod
    def encrypt(password):
        return sha256_crypt.encrypt(password)

    @staticmethod
    def verify(password1, password2):
        if not sha256_crypt.verify(password1, password2):
            raise(Exception("Authentication Error"))
