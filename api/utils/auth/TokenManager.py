from passlib.hash import sha256_crypt
from django.conf import settings

import json
import jwt
import datetime

class TokenManager(object):
    @staticmethod
    def create(userId):
        return jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=10800),
            'iss': settings.SECRET_ISS,
            'id': userId
        }, 
            settings.SECRET_KEY,
            algorithm='HS256'
        )

    @staticmethod
    def verify(token, userId):
        jwt.decode(token, settings.SECRET_KEY, algorithms='HS256', id=userId, iss=settings.SECRET_ISS)
