class IntendedException(Exception):
    """Exception raised for intentional errors."""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)