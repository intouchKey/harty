from django.conf import settings

from api.utils.common.HartyResponse import HartyResponse
from api.utils.exceptions.IntendedException import IntendedException
from functools import wraps

def exception_handler(func):
    def inner_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except IntendedException as e:
            return HartyResponse.createIntended400(e)
        except Exception as e:
            return HartyResponse.create400(e)

    return inner_function
