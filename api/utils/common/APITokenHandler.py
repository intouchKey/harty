from django.conf import settings

from api.utils.common.HartyResponse import HartyResponse
from api.utils.exceptions.IntendedException import IntendedException
from functools import wraps

def api_token_handler(func):
    def inner_function(*args, **kwargs):
        extract_token(*args)
        return func(*args, **kwargs)

    return inner_function
    
def extract_token(_, request):
    token = request.headers['API-TOKEN']

    if not token == settings.API_TOKEN:
        raise(IntendedException("API-TOKEN error"))
