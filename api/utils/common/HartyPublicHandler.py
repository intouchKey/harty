from django.conf import settings

from api.utils.common.HartyResponse import HartyResponse
from api.utils.exceptions.IntendedException import IntendedException

def harty_public_handler(func):
    def inner_function(*args, **kwargs):
        try:
            extract_bearer_token(*args)
            func(*args, **kwargs)
        except IntendedException as e:
            return HartyResponse.createIntended400(e)
        except Exception as e:
            return HartyResponse.create400(e)
    return inner_function
    
def extract_bearer_token(_, request):
    pass