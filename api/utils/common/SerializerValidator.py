class SerializerValidator(object):
    @staticmethod
    def validate(serializer):
        if not serializer.is_valid():
            raise(Exception(serializer.errors))
