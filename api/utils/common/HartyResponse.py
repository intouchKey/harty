import ast

from rest_framework.response import Response
from rest_framework import generics, status

class HartyResponse(object):
    @staticmethod
    def create201(response):
        return Response(response, status=status.HTTP_201_CREATED)

    @staticmethod
    def create200(response):
        return Response(response, status=status.HTTP_200_OK)

    @staticmethod
    def create400(error):
        if "already exists" in str(error):
            return Response({
                "message": str(error),
                "exist": str(error).split("'")[1], 
                "error": True,
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response({
            "message": str(error), 
            "error": True
        }, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def createIntended400(error):
        return Response({
            "message": str(error), 
            "error": True,
            "show": True
        }, status=status.HTTP_400_BAD_REQUEST)
