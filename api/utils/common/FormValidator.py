from api.utils.exceptions.IntendedException import IntendedException

class FormValidator(object):
    @staticmethod
    def create(form, data):
        data = form(data)

        FormValidator.validate(data)
        return data
    @staticmethod
    def validate(form):
        if not form.is_valid():
            raise(IntendedException("Validation error"))
