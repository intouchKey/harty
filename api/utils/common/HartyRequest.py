import ast

from rest_framework.response import Response
from rest_framework import generics, status

class HartyRequest(object):
    @staticmethod
    def getQueryParam(param, request):
        try:
            return request.GET[param]
        except:
            return None
