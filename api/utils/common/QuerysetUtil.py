class QuerysetUtil(object):
    @staticmethod
    def getAll(queryset):
        return queryset.values()

    @staticmethod
    def getOne(queryset, objectName):
        if len(queryset) == 0:
            raise(Exception(objectName + ' not found.'))

        return queryset[0]

    @staticmethod
    def getFirst(queryset, attribute, objectName):
        if len(queryset) == 0:
            raise(Exception(objectName + ' not found.'))
        elif len(queryset) > 1:
            raise(Exception('Invalid schema'))
        
        return getattr(queryset[0], attribute)
