from django.conf import settings

from api.utils.common.HartyResponse import HartyResponse
from api.utils.exceptions.IntendedException import IntendedException
from api.utils.auth.TokenManager import TokenManager
from functools import wraps

def bearer_token_handler(func):
    def inner_function(*args, **kwargs):
        extract_token(*args)
        return func(*args, **kwargs)

    return inner_function
    
def extract_token(_, request):
    userId = request.headers['user-id']
    bearerString = request.headers['Authorization']
    bearerToken = bearerString.split(" ")[1]
    request.data["user_id"] = userId
    
    TokenManager.verify(bearerToken, userId)
