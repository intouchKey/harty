from rest_framework.routers import Route, DynamicRoute, SimpleRouter, DefaultRouter

from django.urls import path, include

from api.views.party.ActivityViewSet import ActivityViewSet

activity_view = ActivityViewSet.as_view({
    'get': 'getAllActivities'
})

activityPatterns = [
    path('activity/', activity_view, name="login"),
]
