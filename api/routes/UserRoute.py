from rest_framework.routers import Route, DynamicRoute, SimpleRouter, DefaultRouter

from django.urls import path, include

from api.views.user.UserViewSet import UserViewSet

user_view = UserViewSet.as_view({
    'get': 'getInfo',
    'put': 'updateUser'
})

user_party_created_view = UserViewSet.as_view({
    'get': 'getCreatedParties'
})

user_party_joined_view = UserViewSet.as_view({
    'get': 'getJoinedParties'
})

user_party_requested_view = UserViewSet.as_view({
    'get': 'getRequestedParties'
})

user_party_created_info_view = UserViewSet.as_view({
    'get': 'getCreatedPartiesWithRequestedUsers'
})

userPatterns = [
    path('user/', user_view, name="user"),
    path('user/party/created/', user_party_created_view, name="user"),
    path('user/party/joined/', user_party_joined_view, name="user"),
    path('user/party/requested/', user_party_requested_view, name="user"),
    path('user/party/created/info/', user_party_created_info_view, name="user"),
]
