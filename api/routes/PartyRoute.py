from rest_framework.routers import Route, DynamicRoute, SimpleRouter, DefaultRouter

from django.urls import path, include

from api.views.party.PartyViewSet import PartyViewSet

party_view = PartyViewSet.as_view({
    'get': 'getAllParties',
    'post': 'create'
})

party_request_view = PartyViewSet.as_view({
    'post': 'requestToJoin'
})

party_accept_view = PartyViewSet.as_view({
    'post': 'acceptRequest'
})

party_decline_view = PartyViewSet.as_view({
    'post': 'declineRequest'
})

party_user_requested_view = PartyViewSet.as_view({
    'get': 'getRequestedUsers'
})

party_user_joined_view = PartyViewSet.as_view({
    'get': 'getJoinedUsers'
})

partyPatterns = [
    path('party/', party_view, name="party"),
    path('party/request/', party_request_view, name="party"),
    path('party/accept/', party_accept_view, name="party"),
    path('party/decline/', party_decline_view, name="party"),
    path('party/user/requested/', party_user_requested_view, name="party"),
    path('party/user/joined/', party_user_joined_view, name="party"),
]
