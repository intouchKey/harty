from rest_framework.routers import Route, DynamicRoute, SimpleRouter, DefaultRouter

from django.urls import path, include

from api.views.auth.LoginViewSet import LoginViewSet
from api.views.auth.RegisterViewSet import RegisterViewSet

login_view = LoginViewSet.as_view({
    'post': 'login'
})

register_view = RegisterViewSet.as_view({
    'get': 'sendVerificationCode',
    'post': 'createAccount',
    'put': 'verifyAccount'
})

authPatterns = [
    path('login/', login_view, name="login"),
    path('register/', register_view, name="register")
]
