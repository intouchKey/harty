from api.models.auth.Auth import Auth
from api.models.user.User import User

from rest_framework import serializers

class AuthSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Auth
        fields = ('id', 'username', 'password', 'date_created', 'email', 'verification_code', 'verified', 'user')
        read_only_fields = ('id', 'date_created')
