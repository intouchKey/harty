from api.models.party.Party import Party
from api.models.user.User import User

from rest_framework import serializers

class PartySerializer(serializers.ModelSerializer):
    class Meta:
        model = Party
        fields = ('id', 'name', 'capacity', 'members_no', 'description', 'location_description', 'is_full', 'date', 'expiration_date', 'date_created', 'date_modified', 'created_user', 'locked_activity', 'province')
        read_only_fields = ('id', 'date_created', 'date_modified')