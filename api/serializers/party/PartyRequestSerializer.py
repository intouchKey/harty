from api.models.party.PartyRequest import PartyRequest

from rest_framework import serializers

class PartyRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartyRequest
        fields = ('party', 'user')
