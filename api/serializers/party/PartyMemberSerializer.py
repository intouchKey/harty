from api.models.party.PartyMember import PartyMember

from rest_framework import serializers

class PartyMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartyMember
        fields = ('party', 'user')
