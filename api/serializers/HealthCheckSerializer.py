from api.models.Test import Test

from rest_framework import serializers

class HealthCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ('title', 'content', 'is_featured')
