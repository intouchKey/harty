from api.models.user.User import User

from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'gender', 'age', 'line_id', 'facebook_name', 'telephone_number')
        read_only_fields = ('id',)
