from django.contrib import admin
from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.models.party.Category import Category
from api.models.party.LockedActivity import LockedActivity
from api.models.party.Party import Party
from api.models.party.Picture import Picture
from api.models.party.Province import Province
from api.models.party.PartyMember import PartyMember
from api.models.party.PartyRequest import PartyRequest

admin.site.register(Auth)
admin.site.register(User)
admin.site.register(Category)
admin.site.register(LockedActivity)
admin.site.register(Party)
admin.site.register(Picture)
admin.site.register(Province)
admin.site.register(PartyMember)
admin.site.register(PartyRequest)
