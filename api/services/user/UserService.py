from api.models.user.User import User
from api.models.party.Party import Party
from api.models.party.PartyMember import PartyMember
from api.models.party.PartyRequest import PartyRequest

from api.utils.common.SerializerValidator import SerializerValidator
from api.serializers.user.UserSerializer import UserSerializer
from api.serializers.party.PartyMemberSerializer import PartyMemberSerializer

from api.utils.common.QuerysetUtil import QuerysetUtil

class UserService(object):
    @staticmethod
    def getPartyIds(object):
        return object.party_id

    @staticmethod
    def mapData(object):
        return object.getData()

    @staticmethod
    def mapPartyRequestToUserInfo(object):
        return object.getUserInfo()

    @staticmethod
    def mapDataWithUserInfo(object):
        data = object.getData()
        
        partyRequests = PartyRequest.objects.filter(party=data["id"])
        requestedUsers = list(map(UserService.mapPartyRequestToUserInfo, partyRequests))

        data["requested_users"] = requestedUsers

        return data

    @staticmethod
    def getInfo(userId):
        queryset = User.objects.filter(id=userId).first()
        
        return queryset.getData()

    @staticmethod
    def getCreatedParties(userId):
        queryset = Party.objects.filter(created_user=userId)

        return list(map(UserService.mapData, queryset))

    @staticmethod
    def getJoinedParties(userId):
        partyMembers = PartyMember.objects.filter(user=userId)
        partyIds = map(UserService.getPartyIds, partyMembers)
        queryset = Party.objects.filter(id__in=partyIds)

        return list(map(UserService.mapData, queryset))

    @staticmethod
    def getRequestedParties(userId):
        partyRequests = PartyRequest.objects.filter(user=userId)
        partyIds = map(UserService.getPartyIds, partyRequests)
        queryset = Party.objects.filter(id__in=partyIds)

        return list(map(UserService.mapData, queryset))

    @staticmethod
    def getCreatedPartiesWithRequestedUsers(userId):
        queryset = Party.objects.filter(created_user=userId)

        return list(map(UserService.mapDataWithUserInfo, queryset))

    @staticmethod
    def update(userId, firstName, lastName, age, gender, lineId, facebookName, telephoneNumber):
        queryset = User.objects.filter(id=userId)

        serializer = UserSerializer(QuerysetUtil.getOne(queryset, "User"), data={
            'first_name': firstName,
            'last_name': lastName,
            'age': age,
            'gender': gender,
            'line_id': lineId,
            'facebook_name': facebookName,
            'telephone_number': telephoneNumber
        }, partial=True)

        SerializerValidator.validate(serializer)
        serializer.save()

        return {
            "message": "Update successfully",
            "error": False
        }
