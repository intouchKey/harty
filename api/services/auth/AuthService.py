from api.models.auth.Auth import Auth
from api.models.user.User import User
from api.serializers.auth.AuthSerializer import AuthSerializer
from api.serializers.user.UserSerializer import UserSerializer

from django.shortcuts import render, get_object_or_404
from django.db import transaction
from django.conf import settings
from django.http import HttpResponse

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from api.utils.common.QuerysetUtil import QuerysetUtil
from api.utils.auth.MailSender import MailSender
from api.utils.common.SerializerValidator import SerializerValidator
from api.utils.auth.PasswordManager import PasswordManager
from api.utils.auth.TokenManager import TokenManager
from api.utils.exceptions.IntendedException import IntendedException

class AuthService(object):
    @staticmethod
    def login(username, password):
        queryset = Auth.objects.filter(username=username)
        databasePassword = QuerysetUtil.getFirst(queryset, "password", "User")
        verified = QuerysetUtil.getFirst(queryset, "verified", "User")

        if not verified:
            raise(IntendedException("Unverified account"))

        PasswordManager.verify(password, databasePassword)

        user = QuerysetUtil.getFirst(queryset, "user", "User")
        token = TokenManager.create(user.getId())

        return {
            "message": "Login successfully", 
            "token": token, 
            "user": user.getData(), 
            "error": False
        }

    @staticmethod
    @transaction.atomic
    def register(username, password, email, firstName, lastName, age, gender):
        verificationCode = MailSender.generateVerificationCode()
        hashedPassword = PasswordManager.encrypt(password)
        userSerializer = UserSerializer(data={
            'first_name': firstName,
            'last_name': lastName,
            'age': age,
            "gender": gender
        })

        SerializerValidator.validate(userSerializer)
        userSerializer.save()
        
        serializer = AuthSerializer(data={
            'username': username, 
            'password': hashedPassword,
            'email': email,
            'verification_code': verificationCode,
            'user': userSerializer.data['id']
        })

        SerializerValidator.validate(serializer)
        serializer.save()

        return {
            "message": "Account created", 
            "username": serializer.data.get("username"), 
            "error": False
        }

    @staticmethod
    def verify(username, verificationCode):
        queryset = Auth.objects.filter(username=username)
        databaseVerificationCode = QuerysetUtil.getFirst(queryset, "verification_code", "User")
        verified = QuerysetUtil.getFirst(queryset, "verified", "User")

        if verified == True:
            raise(IntendedException("Account already verified"))

        if not verificationCode == databaseVerificationCode:
            raise(IntendedException("Invalid verification code"))

        serializer = AuthSerializer(QuerysetUtil.getOne(queryset, "User"), data={
            'verified': True
        }, partial=True)

        SerializerValidator.validate(serializer)
        serializer.save()

        return {
            "message": "Account verified", 
            "username": username, 
            "error": False
        }
    
    @staticmethod
    def sendVerificationCode(username):
        queryset = Auth.objects.filter(username=username)
        email = QuerysetUtil.getFirst(queryset, "email", "User")
        verificationCode = QuerysetUtil.getFirst(queryset, "verification_code", "User")

        MailSender.sendVerificationCode(email, verificationCode)

        return {
            "message": "success", 
            "error": False
        }
