import json

from django.db import transaction

from api.models.user.User import User
from api.models.party.Party import Party
from api.models.party.PartyRequest import PartyRequest
from api.models.party.PartyMember import PartyMember
from api.models.party.Province import Province
from api.models.party.LockedActivity import LockedActivity

from api.utils.common.SerializerValidator import SerializerValidator
from api.serializers.party.PartySerializer import PartySerializer
from api.serializers.party.PartyRequestSerializer import PartyRequestSerializer
from api.serializers.party.PartyMemberSerializer import PartyMemberSerializer
from api.utils.common.QuerysetUtil import QuerysetUtil

class PartyService(object):
    @staticmethod
    def mapData(object):
        return object.getData()

    @staticmethod
    def getUserIds(object):
        return object.user_id

    @staticmethod
    def getAll():
        queryset = Party.objects.all()

        return list(map(PartyService.mapData, queryset))

    @staticmethod
    def getByActivityId(activityId):
        queryset = Party.objects.filter(locked_activity=activityId)

        return list(map(PartyService.mapData, queryset))

    @staticmethod
    def getRequestedUsers(partyId):
        partyMembers = PartyRequest.objects.filter(party=partyId)
        userIds = map(PartyService.getUserIds, partyMembers)
        queryset = User.objects.filter(id__in=userIds)

        return list(map(PartyService.mapData, queryset))

    @staticmethod
    def getJoinedUsers(partyId):
        partyMembers = PartyMember.objects.filter(party=partyId)
        userIds = map(PartyService.getUserIds, partyMembers)
        queryset = User.objects.filter(id__in=userIds)

        return list(map(PartyService.mapData, queryset))

    @staticmethod
    @transaction.atomic
    def create(userId, name, capacity, description, locationDescription, date, expirationDate, province, lockedActivity):
        provinceFound = Province.objects.filter(name=province).first()

        if (provinceFound == None):
            raise(Exception("Province not found"))

        serializer = PartySerializer(data={
            'name': name,
            'capacity': capacity,
            'description': description,
            'location_description': locationDescription,
            'date': date,
            'expiration_date': expirationDate,
            'created_user': userId,
            'locked_activity': lockedActivity,
            'province': provinceFound.pk
        }, partial=True)
        
        SerializerValidator.validate(serializer)
        newParty = serializer.save()
        
        partyMemberSerializer = PartyMemberSerializer(data={
            'user': userId,
            'party': newParty.id
        })

        SerializerValidator.validate(partyMemberSerializer)
        partyMemberSerializer.save()

        return {
            "message": "Created successfully",
            "error": False
        }

    @staticmethod
    def requestToJoin(userId, partyId):
        party = Party.objects.filter(id=partyId).first()
        
        if (party.is_full):
            raise(Exception("The party is full"))

        if (party.created_user.getId() == int(userId)):
            raise(Exception("You can't join the room you created."))

        partyRequests = PartyRequest.objects.filter(user=userId, party=partyId)

        if(len(partyRequests.values()) > 0):
            raise(Exception("Duplicate request"))

        serializer = PartyRequestSerializer(data={
            'user': userId,
            'party': partyId
        })

        SerializerValidator.validate(serializer)
        serializer.save()

        return {
            "message": "Requested to join successfully",
            "error": False
        }

    @staticmethod
    @transaction.atomic
    def acceptRequest(userId, acceptedUserId, partyId):
        party = Party.objects.filter(id=partyId).first()
        
        if (party.is_full):
            raise(Exception("The party is full"))

        if (int(userId) != party.created_user.getId()):
            raise(Exception("Unathorized access"))

        serializer = PartyMemberSerializer(data={
            'user': acceptedUserId,
            'party': partyId
        })

        SerializerValidator.validate(serializer)
        serializer.save()

        party.members_no += 1

        if (party.members_no == party.capacity):
            party.is_full = True
        
        party.save()

        PartyRequest.objects.filter(party=partyId, user=acceptedUserId).delete()

        return {
            "message": "Accepted successfully",
            "error": False
        }

    @staticmethod
    @transaction.atomic
    def declineRequest(userId, declinedUserId, partyId):
        party = Party.objects.filter(id=partyId).first()

        if (int(userId) != party.created_user.getId()):
            raise(Exception("Unathorized access"))

        PartyRequest.objects.filter(party=partyId, user=declinedUserId).delete()

        return {
            "message": "Declined successfully",
            "error": False
        }
