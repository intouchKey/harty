import json

from django.db import transaction

from api.models.user.User import User
from api.models.party.Party import Party
from api.models.party.LockedActivity import LockedActivity
from api.models.party.PartyRequest import PartyRequest
from api.models.party.Province import Province

from api.utils.common.SerializerValidator import SerializerValidator
from api.serializers.party.PartySerializer import PartySerializer
from api.serializers.party.PartyRequestSerializer import PartyRequestSerializer
from api.serializers.party.PartyMemberSerializer import PartyMemberSerializer
from api.utils.common.QuerysetUtil import QuerysetUtil

class ActivityService(object):
    @staticmethod
    def mapData(object):
        return object.getData()

    @staticmethod
    def getAll():
        queryset = LockedActivity.objects.all()

        return list(map(ActivityService.mapData, queryset))
