# Generated by Django 3.1 on 2020-08-25 09:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20200824_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='auth',
            name='user_id',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
