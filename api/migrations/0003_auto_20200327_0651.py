# Generated by Django 2.1.14 on 2020-03-27 06:51

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auth'),
    ]

    operations = [
        migrations.AddField(
            model_name='auth',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='auth',
            name='date_modified',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='auth',
            name='username',
            field=models.CharField(max_length=16, unique=True),
        ),
    ]
