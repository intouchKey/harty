# Dev website

[DEV WEBSITE](https://harty-286207.an.r.appspot.com/api/)

[SENTRY](https://sentry.io/organizations/harty/projects/)

[DOC](https://harty-286207.an.r.appspot.com/api/doc)

[ADMIN](https://harty-286207.an.r.appspot.com/admin)

[HEALTHCHECK](https://harty-286207.an.r.appspot.com/ht/)

To access DOC, you need to go to admin page and login

## Setting Up

1. Start and activate environment

        Virtualenv env
        source env/bin/activate

1. Run the requirements

        pip3 install -r requirements.txt

1. If there is an error installing psycopg2, install psycopg2 driver manually first

        pip install Django psycopg2

1. Run the tests to ensure the project is up and running correctly

        python3 manage.py test

## Creating local database

1. docker run --name harty -p 5432:5432 -e POSTGRES_PASSWORD=2w3e4r5t -d postgres
2. docker container exec -it harty psql -U postgres
3. CREATE USER harty SUPERUSER PASSWORD '2w3e4r5t';
4. CREATE DATABASE harty;
5. GRANT ALL PRIVILEGES ON DATABASE harty to harty;

## Creating migrations

        python3 manage.py makemigrations

## database migration on local

        python3 manage.py migrate

## ! Important ! You must do this step before merging into dev if you created new migrations
## YOU MUST DO THIS ONLY WHEN YOU ARE GOING TO MERGE AND YOU MUST DO THIS BEFORE MERGING
## database migration on dev -> you cannot undo migration so please be sure it is correct

        ./migrate-on-dev.sh

## Connecting to production database using cloud_sql_proxy

./cloud_sql_proxy -instances="harty-286207:asia-southeast1:harty"=tcp:3306

## Collecting static files for publishing

python3 manage.py collectstatic
